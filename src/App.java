public class App {
    public static void main(String[] args) throws Exception {
        Persona objProfesor = new Profesor("Juan", "Perez", "1234");
        Persona objAlumno = new Alumno("Pedro", "Hernan", "987654");
        System.out.println(objProfesor);
        System.out.println(objAlumno);
    }
}
