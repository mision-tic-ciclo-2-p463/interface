public class Profesor implements Persona{
    /************
     * Atributos
     ************/
    private String nombre;
    private String apellido;
    private String cedula;
    private double salario;
    private String materia;

     /************
     * Constructor
     ************/
    public Profesor(String nombre, String apellido, String cedula){
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
    }

     /************
     * Getters 
     * and 
     * Setters
     ************/

    

     /************
     * Acciones
     ************/
    public void asignar_salon(){
        System.out.println("Salon asignado");
    }

    @Override
    public String toString() {
        return "Profesor [apellido=" + apellido + ", cedula=" + cedula + ", nombre=" + nombre + "]";
    }
}